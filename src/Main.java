import java.util.AbstractMap;
import java.util.HashMap;

public class Main {



    public static void main(String[] args) {
        HashMap<Integer, Integer[]> hashMap = new HashMap();
        hashMap.put(20, new Integer[]{10,20,30});
        hashMap.put(21, new Integer[]{10,20,30});
        AbstractMap.SimpleEntry<Integer, Integer> currentTime = new AbstractMap.SimpleEntry<Integer, Integer>(20, 37);
        AbstractMap.SimpleEntry check = check(hashMap, currentTime);
        System.out.println(check);
    }

    private static AbstractMap.SimpleEntry check(HashMap<Integer, Integer[]> hashMap, AbstractMap.SimpleEntry<Integer, Integer> currentTime) {
        Integer[] minutes = hashMap.get(currentTime.getKey());
        if (minutes != null) {
            for (Integer minute : minutes) {
                if (currentTime.getValue() < minute) {
                    return new AbstractMap.SimpleEntry(currentTime.getKey(), minute);
                }
            }
        }
        return checkWithPlusHour(hashMap, currentTime.getKey()+1, currentTime.getValue());
    }

    private static AbstractMap.SimpleEntry checkWithPlusHour(HashMap<Integer, Integer[]> hashMap, Integer hour, Integer minute) {
        Integer[] minutes = hashMap.get(hour);
        if (minutes != null) {
            return new AbstractMap.SimpleEntry(hour, minutes[0]);
        } else {
            return checkWithPlusHour(hashMap, hour+1, minute);
        }
    }
}
